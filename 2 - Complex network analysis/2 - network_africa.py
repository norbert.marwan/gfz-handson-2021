## Climate network example
# Precipitation of Africa, CRU data
# regular grid 0.5° x 0.5°

import numpy as np
import scipy as sc
import networkx as nx
import matplotlib.pyplot as plt
import time

plt.ion()

# load and prepare data
pos = np.loadtxt('pos_africa.txt'); # load positions

p = np.loadtxt('prcp_africa_Jan.txt').transpose(); # load precipitation data
mon = 'Jan'

p = np.loadtxt('prcp_africa_Jul.txt').transpose(); # load precipitation data
mon = 'Jul'

# plot stations
plt.scatter(pos[:,0], pos[:,1])

# map data

# create meshgrid for position
sampling = 1
lat = np.arange(min(pos[:,0]),max(pos[:,0]),sampling)
lon = np.arange(min(pos[:,1]),max(pos[:,1]),sampling)
x, y = np.meshgrid(lat,lon)

# create mask to remove non-land data
mask = np.nan * np.ones(np.shape(x))
for i in range(0,len(pos)):
   idx  = np.argwhere(np.logical_and(x == pos[i,0] , y == pos[i,1]))
   if idx.any():
      mask[idx[0][0],idx[0][1]] = 1;

plt.clf
plt.imshow(mask)

# reshape data to a grid and use median value
from scipy.interpolate import griddata
z = griddata(pos, np.median(p,axis=1), (x, y));

plt.clf() 
plt.pcolor(x, y, z * mask)
plt.title(mon)



# correlation between all locations
C = np.corrcoef(p)
plt.clf()
plt.imshow(C)

# determine the threshold for given link density
np.fill_diagonal(C,0) # remove self-links
th = np.quantile(abs(C), .995) # 0.5% link density

# unidrected, unweighted network
A = abs(C) > th

k = sum(A)

# degree distribution
h, bins = np.histogram(k, bins = max(k))
plt.clf()
plt.loglog(bins[:-1],h)


# plot degree at a map
dz = griddata(pos, k, (x, y));

dz = dz * mask;
plt.clf()
plt.pcolor(x,y,dz,shading='auto')
plt.colorbar()
plt.title(mon)



# create a graph object
G = nx.from_numpy_matrix(A)



# betweenness
start = time.process_time()
b = nx.betweenness_centrality(G, normalized=False, k = 200)
print(time.process_time() - start)

z = griddata(pos, np.log10(np.array(list(b.values())) + 1.), (x, y));

z = z * mask
plt.clf()
plt.pcolor(x,y,z,shading='auto')
plt.title(mon)

plt.colorbar()


# plot using Basemap

from mpl_toolkits.basemap import Basemap
plt.clf()
map = Basemap(llcrnrlon=-20.,llcrnrlat=-40.,urcrnrlon=60.,urcrnrlat=45.)
map.drawcoastlines(linewidth=0.25)
map.drawcountries(linewidth=0.25)
map.drawparallels(np.arange(-90,90,20),labels=[1,1,0,0])
map.drawmeridians(np.arange(-40,120,20),labels=[0,0,0,1])

cs = map.pcolormesh(x,y,z,shading='flat',latlon=True)
plt.title(mon)




# geodesic link length
import great_circ_dist

pos_rad = np.radians(pos) # lat/lon in radians

a_dist = np.zeros(np.shape(A))
for i in range(1,len(A)):
   idx = np.where(A[i,:] == 1)[0] # neighbours of node i
   lon1 = pos_rad[i,0]
   lat1 = pos_rad[i,1]
   lon2 = pos_rad[idx,0]
   lat2 = pos_rad[idx,1]
   a_dist[i,idx] = great_circ_dist.great_circ_dist(lat1, lon1, lat2, lon2)


a_dist2 = a_dist[a_dist != 0] #  use only lengths > 0

# Link length distribution
h,b = np.histogram(a_dist2,100)

plt.clf()
plt.loglog(b[0:-1],h,'.')
plt.xlabel('Link length (km)'), plt.ylabel('Frequency')

# average link distance

avg_ldist = np.zeros(len(A))
max_ldist = np.zeros(len(A))
for i in range(1,len(A)):
   avg_ldist[i] = np.mean(a_dist[i,:]);
   max_ldist[i] = np.max(a_dist[i,:]);


dz = griddata(pos, max_ldist, (x, y));
dz = dz * mask


plt.clf()
map = Basemap(llcrnrlon=-20.,llcrnrlat=-40.,urcrnrlon=60.,urcrnrlat=45.)
map.drawcoastlines(linewidth=0.25)
map.drawcountries(linewidth=0.25)
map.drawparallels(np.arange(-90,90,20),labels=[1,1,0,0])
map.drawmeridians(np.arange(-40,120,20),labels=[0,0,0,1])

cs = map.pcolormesh(x,y,dz,shading='auto',latlon=True)
plt.title(mon)

dict_lon = dict(enumerate(pos[:,0], 1))
dict_lat = dict(enumerate(pos[:,1], 1))

# save network to file
nx.set_node_attributes(G, b, 'betweenness')
nx.set_node_attributes(G, dict_lon, 'lon')
nx.set_node_attributes(G, dict_lat, 'lat')

#dict_dist = dict(enumerate(a_dist.flatten(), 1))
#nx.set_edge_attributes(G, dict_dist, 'linkdistance')

nx.write_graphml(G, "net_africa.graphml")

