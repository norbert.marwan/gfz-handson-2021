%% Demonstration of networks

%% Adjacency matrix
A = [0 0 1 1; 
     0 0 1 0; 
     1 1 0 1;
     1 0 1 0];

imagesc(A)

%% Degree
sum(A)

%% Network
% A has to be logical for an unweighted network, else it will be a weighted network
G = graph(logical(A));

%% Plot network
plot(G)

%% Add nodes
G = addnode(G,2)

numnodes(G)
plot(G)


%% Add edges
numedges(G)
G = addedge(G,1,5)
plot(G)

% using tables
e = table([2 6; 6 1])

% will throw error
addedge(G,e)

% see variable names
e.Properties.VariableNames

% change variable name to 'EndNodes'
e.Properties.VariableNames={'EndNodes'}

G = addedge(G,e)
plot(G)

%% Add nodes by adding edges
G = addedge(G,[5 7 7 7 8 8 8],[4 1 2 3 1 9 12])

plot(G)


%% Graph object to adjacency matrix
A = adjacency(G);

full(A)


%% Network measures

% degree
kv = sum(A)
kv = sum(full(A))

G.degree

% betweenness
centrality(G,'betweenness')


% clustering
C = diag(full(A)*full(A)*full(A))' ./ (kv .* (kv-1));



% transitivity
A2 = full(A)*full(A);
denom = sum(sum(A2)) - trace(A2); % sum with removed main diagonal
Trans = trace(full(A)*full(A)*full(A))/denom;


%% Degree distribution
hist(G.degree, 0:max(G.degree))




%% Graph visualisation
plot(G, 'Layout', 'force')

plot(G, 'Layout', 'circle')

h = plot(G, 'Layout', 'layered')
layout(h,'force3')
