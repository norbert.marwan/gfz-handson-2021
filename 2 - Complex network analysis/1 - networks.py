## Demonstration of networks

import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

plt.ion()


# Adjacency matrix
A = np.array([[0, 0, 1, 1], 
           [0, 0, 1, 0], 
           [1, 1, 0, 1],
           [1, 0, 1, 0]])

plt.imshow(A)

# Degree
sum(A)

# Network
# A has to be logical for an unweighted network, else it will be a weighted network
G = nx.from_numpy_matrix(A)

# Plot network
plt.clf()
nx.draw(G)

plt.clf()
nx.draw_networkx(G)

# Add nodes
G.add_node(4)
G.add_node(5)
G.nodes()

G.number_of_nodes()

plt.clf()
nx.draw_networkx(G)


# Add edges
G.number_of_edges()
G.add_edge(0,4)

plt.clf()
nx.draw_networkx(G)


# using lists
e = [(1, 5), (5, 0)]

G.add_edges_from(e)

plt.clf()
nx.draw_networkx(G)


# Add nodes by adding edges
G.add_edges_from([(4,3),(6,0),(6,1),(6,2),(7,0),(7,8),(7,11)])

plt.clf()
nx.draw_networkx(G)


# Adjacency matrix
A = nx.convert_matrix.to_numpy_matrix(G);


# Network measures

# degree
kv = np.sum(A, axis = 1)

G.degree()

# betweenness
nx.betweenness_centrality(G)


# clustering
denom = np.multiply(kv, (kv-1))
A3 = np.matmul(A, np.matmul(A, A)) # A * A * A
C = np.divide(np.diag(A3), denom)

nx.clustering(G)

# transitivity
A2 = np.matmul(A, A) # A * A
denom = np.sum(A2) - np.trace(A2) # sum with removed main diagonal
Trans = np.trace(A3) / denom

nx.transitivity(G)

# Degree distribution
h = nx.degree_histogram(G)
b=range(0,len(h))

plt.clf()
plt.bar(b,h)



# Graph visualisation
plt.clf()
pos = nx.spring_layout(G)
nx.draw_networkx(G, pos)

plt.clf()
pos = nx.circular_layout(G)
nx.draw_networkx(G, pos)

plt.clf()
pos = nx.kamada_kawai_layout(G)
nx.draw_networkx(G, pos)

plt.clf()
pos = nx.spiral_layout(G)
nx.draw_networkx(G, pos)

