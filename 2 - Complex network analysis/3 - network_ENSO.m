%% Central-eastern Pacific sea surface temperature network

%%
% - create a climate network from spatial central-eastern Pacific SST data
% - degree centrality for each node of the network
% - betweenness centrality for each node of the network
% - average spatial link length of each node
%
% Hints:
% - data: rows correspond to years and columns correspond to different grid points
% - location: lats.txt and lons.txt
% - adjacency matrix: 540x540, use 80%-quantile
% - to plot a local network property such that values can be identified with 
%   the correct grid point on the map, reshaping the respective 2D-array to 
%   a 3D-array with dimensions (t=50, lat=18, lon=30) can be helpful 
% - using a meshgrid for the latitudes and longitudes will be helpful 
%   for the spatial link length distribution

%%
% The data set is imported.

sst = load('sst_enso.txt');
lats = load('lats.txt');
lons = load('lons.txt');
years = 1962:2011;

%%
% reshape data array
La = 18;
Lo = 30;
sst_latlon = reshape(sst, [50, Lo, La]);
%%
% first glimpse at the data
imagesc(lons,lats,squeeze(sst_latlon(1,:,:))')
axis xy

%% Network construction
% Compute the pairwise cross-correlations. Pick a value for the threshold 
% (80% quantile) and generate the adjacency matrix.

%%
% correlation
a_corr = corrcoef(sst);

%%
% adjacency matrix
a_corr = a_corr - eye(size(a_corr)); % remove self-links

alpha = .8;
thresh = quantile(a_corr(:), alpha); % threshold

a_adj = a_corr > thresh;

%%
% transform into network
G = graph(a_adj);

%% Degree centrality

degrees = centrality(G,'degree');

Degree = reshape(degrees, [Lo, La]);% reshaping the degrees
Degree(~(Degree)) = NaN; % replace missing values with NaN

imagesc(lons,lats,Degree')
axis xy
colorbar
colormap(flipud(french))


%% Betweenness centrality
% The bordering region that surrounds the 'ENSO tongue' that extends from the 
% South American westcoast along the equator works as a mediator for SST
% variability. On the other hand, the core regions of the coherent regions 
% do not act as strong mediators.
betweenness = centrality(G,'betweenness');
Betweenness = reshape(betweenness, [Lo, La]);% reshaping the degrees
Betweenness(~(Betweenness)) = NaN; % replace missing values with NaN

imagesc(lons,lats,log10(Betweenness+1)')
axis xy



%% Closeness centrality
closeness = centrality(G,'closeness');
Closeness = reshape(closeness, [Lo, La]);% reshaping the degrees
Closeness(~(Closeness)) = NaN; % replace missing values with NaN

imagesc(lons,lats,Closeness')
axis xy


%% Average spatial link lengths
% great circle distance

%%
% Generate lat&lon arrays to match each element in the adjacency matrix
% to geographical location using a meshgrid

[a_meshlat, a_meshlon] = meshgrid(lats, lons);
a_mlon = a_meshlon(:);
a_mlat = a_meshlat(:);

R = 6373.0;
a_dist = zeros(size(a_adj));


%%
% Loop over all nodes while ensuring that the spatial distance is only 
% computed for those that are linked in the network

for i = 1:length(a_mlat)
   for j = 1:length(a_mlat)
      if a_adj(i, j) % only if i and j are linked

          lat1 = deg2rad(a_mlat(i));
          lon1 = deg2rad(a_mlon(i));
          lat2 = deg2rad(a_mlat(j));
          lon2 = deg2rad(a_mlon(j));
          % distance computation
          dlon = lon2 - lon1;
          dlat = lat2 - lat1;
          a = sin(dlat / 2)^2 + cos(lat1) * cos(lat2) * sin(dlon / 2)^2;
          if a > 1, a = 1; end
          c = 2 * atan2(sqrt(a), sqrt(1 - a));
          distance = R * c;
          a_dist(i,j) = distance;
      end
   end
end
   

%%
% Compute the average link length from all link lengths of a given node. 

%%
% Apparently, only average over non-zero entries
a_nonzero = a_dist;
a_nonzero(~a_dist) = NaN;

%%
% averaging:
meanlinklen = reshape(nanmean(a_nonzero), [Lo, La]);

%% 
% Plot
imagesc(lons,lats,meanlinklen')
axis xy




%% Link distance
d = distances(G);
d(isinf(d)) = NaN;
d = nanmean(d);
d = reshape(d, [Lo, La]);% reshaping the degrees

imagesc(lons,lats,1./meanlinklen')
axis xy



