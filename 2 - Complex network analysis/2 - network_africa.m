%% Climate network example
% Precipitation of Africa, CRU data
% regular grid 0.5° x 0.5°

%% load and prepare data
clear
pos = load('pos_africa.txt'); % load positions

p = load('prcp_africa_Jan.txt')'; % load precipitation data
p = load('prcp_africa_Jul.txt')'; % load precipitation data

%% plot stations
plot(pos(:,1), pos(:,2), '.')


%% map data

% create meshgrid for position
sampling = 1;
[x y] = meshgrid(min(pos(:,2)):sampling:max(pos(:,2)),min(pos(:,1)):sampling:max(pos(:,1)));

% create mask to remove non-land data
mask = NaN*ones(size(x));
for i = 1:length(pos)
   idx = find(x == pos(i,2) & y == pos(i,1));
   mask(idx) = 1;
end

imagesc(mask)

% reshape data to a grid and use median value
z = griddata(pos(1:end,2),pos(1:end,1), median(p(1:end,:),2), x, y);

clf 
worldmap('africa')
pcolorm(x, y, z .* mask)
geoshow('landareas.shp', 'FaceColor', 'none')

colorbar('horiz')
title('Precipitation (median)','fontw','b')


%% correlation between all locations
%C = corr(p','Type','Spearman');
C = corr(p');
clf
imagesc(C)

%% determine the threshold for given link density
C = C - eye(size(C)); % remove self-links
th = quantile(abs(C(:)), .995); % 0.5% link density
th=.9
%% unidrected, unweighted network
A = abs(C) > th;

d = sum(A);

%% degree distribution
[h bins] = hist(d,0:max(d));
loglog(bins,h,'.')

% %% test whether isolated components
% L = -double(A) - eye(length(A)) + diag(d);
% ev = eig(L);
% ev(2)

%% create a graph object
G = graph(A);

b = centrality(G,'betweenness');

%% degree centrality
d = sum(A);
%d2 = centrality(G,'degree');

%% clustering coeff
%cl = diag(A*A*A)' ./ (d .* (d-1));
%cl2 = centrality(G,'closeness');



dz = griddata(pos(1:sampling:end,2),pos(1:sampling:end,1), log10(b+1), x, y);
%dz = griddata(pos(1:sampling:end,2),pos(1:sampling:end,1), cl, x, y);
dz = griddata(pos(1:sampling:end,2),pos(1:sampling:end,1), d, x, y);
%dz = griddata(pos(1:sampling:end,2),pos(1:sampling:end,1), cl, x, y);
%subplot(222), 
clf

dz = dz .* mask;
worldmap('africa')
pcolorm(x,y,dz), colorbar('horiz')
title('Betweenness Centrality','fontw','b')

geoshow('landareas.shp', 'FaceColor', 'none')

geoshow('worldlakes.shp', 'FaceColor', [ .8 .9 1])
geoshow('worldrivers.shp', 'Color', [ 0 0 0])



%% geodesic link length

pos_rad = deg2rad(pos); % lat/lon in radians
R = 6373.0; % Earth radius

h = waitbar(0,'Geodesic link lenght')
for i = 1:length(A)
   waitbar(i/length(A))
   idx = find(A(i,:)); % neighbours of node i
   lon1 = pos_rad(i,1);
   lat1 = pos_rad(i,2);
   lon2 = pos_rad(idx,1);
   lat2 = pos_rad(idx,2);

   dlon = lon2 - lon1;
   dlat = lat2 - lat1;
   
   % geodesic distance
   a = sin(dlat / 2).^2 + cos(lat1) .* cos(lat2) .* sin(dlon / 2).^2;
   a(a>1) = 1; % ensure to not exceed 1
   c = 2 * atan2(sqrt(a), sqrt(1 - a));
   a_dist(i,idx) = R * c;
end
close(h)

a_dist2 = a_dist(a_dist>0); % use only lengths > 0

% Link length distribution
[h b] = hist(a_dist2,100);
loglog(b,h,'.')
xlabel('Link length (km)'), ylabel('Frequency')

%% average link distance

for i = 1:size(A,2)
   avg_ldist(i) = mean(a_dist(i,:));
   max_ldist(i) = max(a_dist(i,:));
end

dz = griddata(pos(1:sampling:end,2),pos(1:sampling:end,1), avg_ldist, x, y);
dz = griddata(pos(1:sampling:end,2),pos(1:sampling:end,1), max_ldist, x, y);
dz = dz .* mask;

clf
worldmap('africa')
pcolorm(x,y,dz), colorbar('horiz')
title('Average link distance','fontw','b')

geoshow('landareas.shp', 'FaceColor', 'none')
geoshow('worldlakes.shp', 'FaceColor', [ .8 .9 1])
geoshow('worldrivers.shp', 'Color', [ 0 0 0])




%% save network data to file

% save node list
writematrix(['ID;Lon;Lat'],'nodes.csv')
writematrix([(1:length(pos))' pos], 'nodes.csv', 'delimiter', ';','WriteMode','append')


% save edge list
[row col v] = find(sparse(A));
writematrix(['Source;Target'],'edges.csv')
writematrix([row col], 'edges.csv', 'delimiter', ';','WriteMode','append')





