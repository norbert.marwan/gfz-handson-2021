# A function that computes the great circle distance between two locations, given by their latitudes and longitudes
import numpy as np

def great_circ_dist(lat1, lon1, lat2, lon2):
    R = 6373.0
    # distance computation
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = np.sin(dlat / 2.)**2. + np.cos(lat1) * np.cos(lat2) * np.sin(dlon / 2.)**2.
    a = np.where(a <= 1, a, 1)
    c = 2. * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
    distance = R * c
    return distance
