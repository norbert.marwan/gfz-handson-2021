import math
import numpy as np
from sklearn.neighbors import KDTree


def fnn(x, tau, m = 10, rtol=10.0, atol=2.0, theiler=0):
    """Calculate the number of "false nearest neighbors" (FNNs) of the datasets created
    from `x` by time delay embedding `s` up to dimension `m` with lag `tau`. The first 
    value from the output corresponds to the fraction of false nearest neighbors, when going 
    from dimension 1 to an embedding of dimension 2. 

    Variables and Parameters
    ------------------------

    x : `numpy.ndarray` (N,1) or `list` with `len(x)=N`
        Univariate input time series.
    tau : `int`
        Time delay for the embedding.
    m : `int`
        Maximal embedding dimension up to which the FNNs will be computed. 
    rtol : `float` or `int` 
        Relative increment of the distance between the nearest neighbors tolerable before denoted as false
    atol : `float` or `int`
        Threshold for the ratio between the increased distance and the "size of the attractor".
    theiler :  


    Description
    -----------

    Given a dataset made by `embed(x, d, tau)`, the "false nearest neighbors" (FNN) are the pairs of points 
    that are nearest to each other at dimension `d`, but are separated at dimension `d+1`. Kennel's
    criteria for detecting FNN are based on a threshold for the relative increment
    of the distance between the nearest neighbors (`rtol`, eq. 4 in [kennel1992]_), and
    another threshold for the ratio between the increased distance and the
    "size of the attractor" (`atol`, eq. 5 in [kennel1992]_). These thresholds are given
    as keyword arguments.

    The returned value is a vector with the number of FNN for each `d` out of `m`. The
    optimal value for `d` is found at the point where the number of FNN approaches
    zero.

    References
    ----------
    .. [kennel1992] Kennel et al., "Determining embedding dimension for phase-space reconstruction using a geometrical construction", Phys. Rev. A 45, 3403, 1992. https://doi.org/10.1103/PhysRevA.45.3403 
    """

    assert (theiler >= 0) and (type(theiler) is int) and (theiler < len(x)), "Theiler window must be a positive integer smaller than the time series length."
    assert (m > 0) and (type(m)) is int, "Maximal embedding dimension must be a positive integer."
    assert (tau > 0) and (type(tau)) is int, "Time delay must be a positive integer."
    assert (rtol > 0), "Rtol-threshold must be positive."
    assert (atol > 0), "atol-threshold must be positive."
    
    norm = 'euclidean'  # norm for distance computation
    s = np.array(x)
    ds = np.arange(1,m+1)
    rtol2 = rtol**2
    Ra = np.std(s)
    K = 1 #number of nearest neighbors

    nfnn = np.zeros(len(ds))
    for (k, d) in enumerate(ds):
        y = embed(s[:-tau],d,tau)
        ns = [i for i in range(np.size(y,0))] # fiducial point indices
        vs = y[ns] # fiducial points
        if d == 1:
            nind, _ = all_neighbors_1dim(vs, ns, K, theiler)
        else:
            tree = KDTree(y, metric = norm)
            nind, _ = all_neighbors(tree, vs, ns, K, theiler, 10)

        for (i,j) in enumerate(nind):
            delta = np.linalg.norm(y[i]-y[j])
            # If y[i] and y[j] are still identical, choose the next nearest neighbor
            # as in Cao's algorithm (not suggested by Kennel, but still advisable)
            if delta == 0.0:
                _, ind_ = tree.query(y[i], k=2)
                j = ind_[-1]
                delta = np.linalg.norm(y[i]-y[j]) # geht das? norm ist doch ein string mit inhalt "euclidean"
            
            delta_1 = _increase_distance(delta,s,i,j,d-1,tau)
            cond_1 = ((delta_1/delta)**2 - 1 > rtol2) # equation (4) of Kennel
            cond_2 = (delta_1/Ra > atol)         # equation (5) of Kennel
            if cond_1 | cond_2:
                nfnn[k] += 1
            
        nfnn[k] /= len(nind)
    return nfnn,ds


# Helper functions

def embed2(Y,x,tau):
    N = np.size(Y,0)
    MM = len(x)
    MMM = MM - tau
    M = np.amin([N, MMM])
    if np.ndim(Y)==1:
        Y2 = np.transpose([Y[:M], x[tau:tau+M]])
    else:
        Y2 = np.append(Y[:M,:],np.transpose([x[tau:tau+M]]),axis=1)
    return Y2

def embed(s,m,tau):
    Y = s
    for d in np.arange(1,m):
        Y = embed2(Y,s,tau*d)
    return Y

def all_neighbors(vtree, vs, ns, K, theiler, k_max):
    '''Compute `K` nearest neighbours for the points `vs` (having indices `ns`) from the tree `vtree`, while respecting the `theiler`-window.

    Returns
    -------
    indices : `numpy.ndarray` (len(vs),K)
        The indices of the K-nearest neighbours of all points `vs` (having indices `ns`)
    dists : `numpy.ndarray` (len(vs),K)
        The distances to the K-nearest neighbours of all points `vs` (having indices `ns`)
    '''
    dists = np.empty(shape=(len(vs),K))  
    idxs = np.empty(shape=(len(vs),K),dtype=int)

    dist_, ind_ = vtree.query(vs[:], k=k_max)

    for i in range(np.size(dist_,0)):    
        cnt = 0
        for j in range(1,np.size(dist_,1)):
            if ind_[i,j] < ns[i]-theiler or ind_[i,j] > ns[i]+theiler:
                dists[i,cnt], idxs[i,cnt] = dist_[i,j], ind_[i,j]
                if cnt == K-1:
                    break
                else:
                    cnt += 1
    return idxs, dists
    
def all_neighbors_1dim(vs, ns, K, theiler):
    '''Compute `K` nearest neighbours for the points `vs` (having indices `ns`), while respecting the `theiler`-window.

    Returns
    -------
    indices : `numpy.ndarray` (len(vs),K)
        The indices of the K-nearest neighbours of all points `vs` (having indices `ns`)
    dists : `numpy.ndarray` (len(vs),K)
        The distances to the K-nearest neighbours of all points `vs` (having indices `ns`)
    '''
    dists = np.empty(shape=(len(vs),K))  
    idxs = np.empty(shape=(len(vs),K),dtype=int)

    for (i,v) in enumerate(vs):
        dis = np.array([abs(v - vs[j]) for j in range(len(vs))])
        idx = np.argsort(dis)
        cnt = 0
        for j in range(len(idxs)):
            if idx[j] < ns[i]-theiler or idx[j] > ns[i]+theiler:
                dists[i,cnt], idxs[i,cnt] = dis[idx[j]], idx[j]
                if cnt == K-1:
                    break
                else:
                    cnt += 1
    return idxs, dists

def _increase_distance(delta, s, i, j, gamma, tau):
    c = np.absolute(s[i+gamma*tau+tau] - s[j+gamma*tau+tau])
    res = np.sqrt(delta*delta + c*c)
    return res
