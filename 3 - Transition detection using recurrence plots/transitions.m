%% Phase space reconstruction

%% time series of pendulum
t = linspace(0, 2*pi * 10, 634);
x = sin(t)';
y = cos(t)';

% plot time series
plot(t,x, t,y)

% plot time series
plot(x,y)


%% autocorrelation
lag = 200;
c = xcorr(x,lag,'coeff');
plot(-lag:lag, c)
grid on


tau = 16;

%% false nearest neighbours

fnn(x,6,tau)


%% embedding vector
m = 2;
N = length(x) - (m-1)*tau;

xe = zeros(N,m);
for j = 1:m
   xe(:,j) = x([1:N]+tau*(j-1));
end

plot(xe(:,1), xe(:,2))


%% create external function 'embed.m'
xe = embed(x,2,tau);


%% Lorenz data
clear
x = load('lorenz.csv');

plot(x)

%% phase space
plot3(x(:,1),x(:,2),x(:,3))


%% autocorrelation
lag = 500;
c = xcorr(x(:,1),lag,'coeff');
plot(-lag:lag, c)
grid on

% correlation time
line([-500 500],[1 1]./exp(1))


% false nearest neighbours
fnn(x(:,1))


% phase space reconstruction
tau = 4;

plot3(x(1:end-2*tau,1),x(1+tau:end-tau,1),x(1+2*tau:end,1))

%% use mutual information instead of ACF
mi(x(:,1),10,50)


%% Recurrence plot

% RP for a 3-dimensional system
whos x
D = squareform(pdist(x));

% show distance matrix
imagesc(D)

% recurrence matrix
imagesc(D < quantile(D(:),.1))

%% using CRP toolbox
crp2(x(1:end,:)) % for multidimensional vectors

crp(x(1:3:end,1),3,5) % for univariate time series

R = crp(x(1:3:end,1),3,5, 'nogui'); % store RP in variable R
imagesc(R)
colormap([1 1 1; 0 0 0])


% some test models
N = 1000;
x = rand(N,1);
x = sin(linspace(0,2*pi * 10,N));
x = sin(linspace(0,2*pi * 10,N)) + sin(linspace(0,2*pi * 32.4,N));

x = rand(1,1);
for i = 2:N
    x(i) = .99 * x(i-1) + randn(1,1);
end

plot(x)
crp(x)


% some data
x = load('tianmen.csv');
x = load('qunf.csv');
x = load('lonar.csv');

t = x(1,1):5:x(end,1);
xi = interp1(x(:,1),x(:,2),t);

plot(t,xi)


%% Recurrence quantification

% for noise
N = 1000;
x = rand(N,1);

X = crp(x,1,1,.1,'rr');
[l set_l] = dl(X);

bins = 1:20;
hl = hist(set_l,bins);

bar(bins,hl)

% determinism
sum(set_l(set_l > 1)) / sum(set_l)

crqa(x)


% for logistic map
N = 10000;
a = linspace(3.4,4,N)';
x = .451;
for i = 2:N
    x(i,1) = a(i) * x(i-1) * (1 - x(i-1));
end

plot(a,x,'k.')


%% RP
crp([a(1:2000), x(1:2000)])

crp([a(3000:end), x(3000:end)])


%% RQA
crqa([a, x],1,1,.1)