## Phase space reconstruction

import numpy as np
import matplotlib.pyplot as plt

plt.ion()

# time series of pendulum
t = np.linspace(0, 2*np.pi * 10, 634);
x = np.sin(t)
y = np.cos(t)

# plot time series
plt.plot(t,x, t,y)


# plot time series
plt.clf()
plt.plot(x,y)


# autocorrelation
lag = 200
plt.clf()
plt.xcorr(x, x, maxlags=lag)


tau = 16;

# false nearest neighbours
import fnn
f = fnn.fnn(x,6,tau)
plt.clf()
plt.plot(f)


# embedding vector
m = 2
N = len(x) - (m-1)*tau

xe = np.zeros((N,m))
for j in range(0,m):
   xe[:,j] = x[np.arange(0,N)+tau*(j)]

plt.clf()
plt.plot(xe[:,0], xe[:,1])


# create function 'embed'
def embed(x,m,tau):
   N = len(x) - (m-1)*tau
   xe = np.zeros((N,m))
   for j in range(0,m):
      xe[:,j] = x[np.arange(0,N)+tau*(j)]
   return xe

xe = embed(x,2,tau)


# Lorenz data
x = np.loadtxt('lorenz.csv')

plt.plot(x)

plt.plot(x[:,0])

# phase space
ax = plt.axes(projection='3d')
ax.plot(x[:,0],x[:,1],x[:,2])


# autocorrelation
lag = 500
plt.xcorr(x[:,0], x[:,0], maxlags=lag)

# correlation time
plt.plot((-500, 500),(1./np.e, 1/np.e))


# false nearest neighbours
f,m = fnn.fnn(x[1:500,0],15,10)

plt.plot(m,f)


# phase space reconstruction
tau = 4

ax = plt.axes(projection='3d')
ax.plot(x[0:-2*tau-1,0],x[0+tau:-tau-1,0],x[0+2*tau:-1,0])

# use mutual information instead of ACF
import mi
c,lags = mi.mi(x[:,0],100)

plt.plot(lags,c)


# Recurrence plot

# RP for a 3-dimensional system
from scipy.spatial.distance import pdist, squareform

D = squareform(pdist(x))

# show distance matrix
plt.imshow(D)

# recurrence matrix
plt.imshow(D < np.quantile(D,.1), cmap='gray_r')


# define rp as function
def rp(y, m = 1, tau = 1, e = .1):
   if len(np.shape(y)) == 1:
       xe = embed(y, m, tau)
   else:
       xe = y
   D = squareform(pdist(xe))
   return (D < np.quantile(D,e))


R = rp(x[:,0], 3, 4, .1)

plt.imshow(R, cmap='gray_r')


# some test models


N = 1000
x = np.random.normal(0, 1, N)
x = np.sin(np.linspace(0,2*np.pi * 10,N))
x = np.sin(np.linspace(0,2*np.pi * 10,N)) + np.sin(np.linspace(0,2*np.pi * 32.4,N))

x = np.zeros((N,1))
x[0] = .7
for i in range(1,N):
    x[i] = .99 * x[i-1] + np.random.normal()



plt.plot(x)
R = rp(x, 2, 4, .1)
plt.imshow(R, cmap='gray_r')


# some data
x = np.loadtxt('tianmen.csv', delimiter=';')
x = np.loadtxt('qunf.csv', delimiter=';')
x = np.loadtxt('lonar.csv', delimiter=';')

t = np.r_[x[0,0]:x[-1,0]:5]
xi = np.interp(t,x[:,0],x[:,1])

plt.plot(t,xi)


# Recurrence quantification
import rqa

# for noise
N = 1000;
x = np.random.normal(0,1,N)

X = rqa.rp(x,1,1,.1)
set_l = rqa.dl(X)

bins = range(0,20)

plt.clf()
plt.hist(set_l,bins)

# determinism
np.sum(set_l[set_l > 1]) / np.sum(set_l)


# for logistic map
N = 10000
a = np.linspace(3.4,4,N)
x = np.zeros((N,1))
x[0] = .451
for i in range(1,N):
    x[i] = a[i-1] * x[i-1] * (1 - x[i-1])


plt.clf()
plt.scatter(a,x,s=1)


# RP
R1 = rqa.rp(x[0:2000])
R2 = rqa.rp(x[3000:5000])

fig, ax = plt.subplots(1,2)

ax[0].imshow(np.flip(R1, axis=0), cmap='gray_r', extent = [a[0], a[2000], a[0], a[2000]])

ax[1].imshow(np.flip(R2, axis=0), cmap='gray_r', extent = [a[3000], a[4999], a[3000], a[4999]]) 



# windowed RQA
from progress.bar import Bar

w = 200 # window size
ws = 20 # window step

tr = []
d = []
a_ = []

bar = Bar('Windowed analysis', max=(N-w)/ws)
for i in range(0, N-w, ws):
   R = rqa.rp(x[i:i+w], 2, 1, .25)
   tr.append(rqa.trans(R))
   d.append(rqa.det(R))
   a_.append(a[i+int(w/2)])
   bar.next()

bar.finish()


fig, ax = plt.subplots(3)

ax[0].scatter(a,x,s=1)

ax[1].plot(a_,d)
ax[2].plot(a_,tr)



# RQA of CENOGRID data

# load data
cenogrid = np.loadtxt('cenogrid.txt', skiprows=1, delimiter=';').T
t = -np.flip(cenogrid[0,])
d13C = np.flip(cenogrid[1,])
d18O = -np.flip(cenogrid[2,])


# plot data
plt.clf()
plt.plot(t, d13C,linewidth=2, color='darkcyan', label='d13C')
plt.plot(t, d18O,linewidth=2, color='mediumpurple', label='d18O')

plt.legend()


# sampling time change
plt.clf()
plt.plot(t)

# resample using interpolation
t_new = np.arange(t[0],t[-1]-0.005,0.005)
d18Oi = np.interp(t_new, t, d18O)


plt.clf()
plt.plot(t, d18O,linewidth=2, color='darkcyan', label='d18O')
plt.plot(t_new, d18Oi,linewidth=2, color='mediumpurple', label='d18O interp')
plt.xlabel('Time (Ma)')


# windowed RQA
w = 400 # window size
ws =100 # window step

tr = []
d = []
t_ = []
N = len(d18Oi)

bar = Bar('Windowed analysis', max=(N-w)/ws)
for i in range(0, N-w, ws):
   R = rqa.rp(d18Oi[i:i+w], 2, 1, .25)
   tr.append(rqa.trans(R))
   d.append(rqa.det(R))
   t_.append(t_new[i+int(w/2)])
   bar.next()

bar.finish()


fig, ax = plt.subplots(3)

ax[0].plot(t_new,d18Oi)

ax[1].plot(t_,d)
ax[1].set_ylabel('DET')

ax[2].plot(t_,tr)
ax[2].set_ylabel('Trans')
ax[2].set_xlabel('Time (Ma)')
