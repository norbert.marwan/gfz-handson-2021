"""
Import this script as a module 
"""

import numpy as np 
from itertools import chain
from scipy.spatial.distance import pdist, squareform



def trans(A):
    """returns RQA measure transitivity."""
    A2 = np.matmul(A, A)
    A3 = np.matmul(A, np.matmul(A, A))

    denom = np.sum(A2) - np.trace(A2) # sum with removed main diagonal
    return np.divide(np.trace(A3), denom)


def det(R, lmin=2):
    """returns RQA measure determinism."""
    ll = dl(R)
    return np.sum(ll[ll >= lmin]) / np.sum(ll)



def dl(R):
    """returns the histogram P(l) of diagonal lines of length l."""
    line_lengths = []
    for i in range(1, len(R)):
        d = np.diag(R, k=i)
        ll = _count_num_lines(d)
        line_lengths.extend(ll)
    return np.asarray(line_lengths)
    #bins = np.arange(0.5, line_lengths.max() + 0.5, 1.)
    #num_lines, _ = np.histogram(line_lengths, bins=bins)
    #return num_lines, bins, line_lengths



def _count_num_lines(arr):
    """returns a list of line lengths contained in given array."""
    line_lens = []
    counting = False
    l = 0
    for i in range(len(arr)):
        if counting:
            if arr[i] == 0:
                l += 1
                line_lens.append(l)
                l = 0
                counting = False
            elif arr[i] == 1:
                l += 1
                if i == len(arr) - 1:
                    l += 1
                    line_lens.append(l)
        elif not counting:
            if arr[i] == 1:
                counting = True
    return line_lens



# create function 'embed'
def embed(x,m,tau):
   N = len(x) - (m-1)*tau
   xe = np.zeros((N,m))
   for j in range(0,m):
      xe[:,j] = x[np.arange(0,N)+tau*(j)]
   return xe



# define rp as function
def rp(y, m = 1, tau = 1, e = .1):
   if len(np.shape(y)) == 1:
       xe = embed(y, m, tau)
   else:
       xe = y
   D = squareform(pdist(xe))
   return (D < np.quantile(D,e))
