import numpy as np
import matplotlib.pyplot as plt
import rqa

plt.ion()


## CRP
t = np.arange(0, 1000) * 2 * np.pi / 67
a = np.sin(t)
b = np.cos(.01 * t**2)

plt.plot(t,a)
plt.plot(t,b)


m = 3
tau = 12

# embedding
ae = rqa.embed(a,m,tau)
be = rqa.embed(b,m,tau)

# CRP
from scipy.spatial.distance import cdist
D = cdist(ae, be)

plt.clf()
plt.imshow(D)


# JRP

# separate RP for a and b, using 10% RR
Ra = rqa.rp(ae,.1)
Rb = rqa.rp(be,.1)

# the JRP
JR = Ra * Rb

plt.clf()
plt.imshow(JR, cmap='gray_r')


# use modified variable
c = a**2

# linear correlation
np.corrcoef(a,c)

# embedding
ce = rqa.embed(c,m,tau)

# RP for c
Rc = rqa.rp(ce,.1)

# JRP
JR = Ra * Rc

fig, ax = plt.subplots(1,3)
ax[0].imshow(Ra, cmap='gray_r')
ax[1].imshow(Rc, cmap='gray_r')
ax[2].imshow(JR, cmap='gray_r')

np.mean(Ra)
np.mean(JR)


# Phasesync

N = 2000
phaseshift = 2. * np.cos( np.linspace(0, 2*np.pi*10, N) )
ampshift = .8 + .4* np.sin( np.linspace(0, 2*np.pi*15, N) )
t = np.linspace(0, 2*np.pi*44, N)

x = np.sin( t+phaseshift )
y = ampshift * np.cos( t+phaseshift )

plt.clf()
plt.plot(t,x, t,y)
plt.xlabel('Time')
plt.ylabel('x_1, x_2')

np.corrcoef(x,y)


# tau-RR

# embedding
m = 2
tau = 12
xe = rqa.embed(x,m,tau)
ye = rqa.embed(y,m,tau)

# separate RP for a and b, using 10% RR
Rx = rqa.rp(xe)
Ry = rqa.rp(ye)

# diagonalwise RR
maxW = 800

tauRRx = np.zeros(maxW)
tauRRy = np.zeros(maxW)

for k in range(0,maxW):
   tauRRx[k] = np.mean(np.diagonal(Rx,k))
   tauRRy[k] = np.mean(np.diagonal(Ry,k))

plt.clf()
plt.plot(range(0,maxW),tauRRx, range(0,maxW),tauRRy)


tau = 20
np.corrcoef(tauRRx[tau:], tauRRy[tau:])



## CENOGRID example
x = np.loadtxt('cenogrid.txt', skiprows=1, delimiter=';') # load CENOGRID data

t = np.arange(x[1,0], x[-1,0], 0.005) # new time axis
x_ = np.interp(t, x[:,0], x[:,1]) # interpolate to new time axis
x = np.array([1000*t, x_]).T

x = np.flipud(x) # reverse order to start with oldest age


y = np.loadtxt('laskar_67Ma.txt', skiprows=1) # load orbital parameters

plt.plot(x[:,0],x[:,1])

plt.clf()
plt.plot(y[:,0],y[:,1])


#  RP
toi = range(12700,13420)

xe = rqa.embed(x[toi,1], 4, 3)
ye = rqa.embed(y[toi,3], 3, 2)

Rx = rqa.rp(xe)
Ry = rqa.rp(ye)

plt.clf()
plt.imshow(Rx, cmap='gray_r')

plt.clf()
plt.imshow(Ry, cmap='gray_r')

# calculate tauRR
maxW = 120;

tauRRx = np.zeros(maxW)
tauRRy = np.zeros(maxW)

for k in range(0,maxW):
   tauRRx[k] = np.mean(np.diagonal(Rx,k))
   tauRRy[k] = np.mean(np.diagonal(Ry,k))

plt.clf()
plt.plot(range(0,maxW),tauRRx, range(0,maxW),tauRRy)


# CPR measure
tau = 20
np.corrcoef(tauRRx[tau:], tauRRy[tau:])

