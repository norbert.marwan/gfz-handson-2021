# GFZ Complex Systems  Workshop 2021-11-04/05

## Complex networks

Required packages for Python:

- networkx
- basemap
- sklearn

Required toolbox for MATLAB:

- Mapping toolbox could be helpful
 
## Recurrence analysis

Required toolbox for MATLAB:

- CRP Toolbox: <https://tocsy.pik-potsdam.de/CRPtoolbox/>
 
## Hands-on workshop

- data available in the corresponding sub-folders
- some helper functions are also available
- scripts will be uploaded after the workshop